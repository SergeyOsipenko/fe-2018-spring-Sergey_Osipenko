var defObj = {
    x: 6,
    y: 15,
    z: 4
};

function op1(objPar) {
    var objCopy ={};
    Object.assign(objCopy, objPar);
    console.log("Results of the operation x += y - x++ * z:");
    return objCopy.x += objCopy.y - objCopy.x++ * objCopy.z;
}

function op2(objPar) {
    var objCopy ={};
    Object.assign(objCopy, objPar);
    console.log("Results of the operation z = -- x - y * 5:");
    return objCopy.z = -- objCopy.x - objCopy.y * 5;
}

function op3(objPar) {
    var objCopy ={};
    Object.assign(objCopy, objPar);
    console.log("Results of the operation y /= x + 5 % z:");
    return objCopy.y /= objCopy.x + 5 % objCopy.z;
}

function op4(objPar) {
    var objCopy ={};
    Object.assign(objCopy, objPar);
    console.log("Results of the operation z = x++ + y * 5:");
    return objCopy.z = objCopy.x++ + objCopy.y * 5;
}

function op5(objPar) {
    var objCopy ={};
    Object.assign(objCopy, objPar);
    console.log("Results of the operation x = y - x++ * z:");
    return objCopy.x = objCopy.y - objCopy.x++ * objCopy.z;
}

console.log("Parametrs of the operations: x = " + defObj.x + ", y = " + defObj.y + ", z = " + defObj.z);
console.log(op1(defObj));
console.log(op2(defObj));
console.log(op3(defObj));
console.log(op4(defObj));
console.log(op5(defObj));