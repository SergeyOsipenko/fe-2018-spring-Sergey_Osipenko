const PI = 3.14;

function calcVolumeOfCil(r, h) {
    var v;
    v = PI * r * 2 * h;
    return v;
}

function calcSurfaceOfCil(r, h) {
    var s;
    s = 2 * PI * r * (r + h);
    return s;
}

console.log("Volume of the cylinder = " + calcVolumeOfCil(10, 10));
console.log("Surface area of the cylinder = " + calcSurfaceOfCil(10, 10));