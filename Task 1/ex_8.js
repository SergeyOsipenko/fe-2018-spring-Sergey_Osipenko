var arr = createArray(5, 5);
console.log("Array before change:");
console.log(arr);
console.log();
changeArray(arr);
console.log("Array after change:");
console.log(arr);

function createArray(r, c) {
    var arr = [];
    for (var i = 0; i < r; i++) {
        arr[i] = [];
    for (var j = 0; j < c; j++) {
        arr[i][j] = Math.floor(-10 + Math.random()*(10 + 1 - (-10)));
        }
    }
    return arr;
}

function changeArray(arr) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i][i] < 0) {
            arr[i][i] = 10;
        } else if (arr[i][i] > 0) {
            arr[i][i] = 20;
        }	
    }
    return arr;
}