rectangle(10, 8);
rightTriangle();
equilateralTriangle();
rhombus();

function rectangle(a, b) {
    var str = '';
	for (var i = 0; i < a; i++) {
		str += '*';
	}

	for (var j = 0; j < b; j++) {
		console.log(str);
	}
    console.log("---------------------");
}

function rightTriangle() {
    var lines = 10;
    var str = " ";
    var star = "*";
    for(var i = 0; i < lines; i++){
        str += star;
        console.log(str);
    }
    console.log("---------------------");
}

function equilateralTriangle() {
    var y = 0;
    var z = 0;
    var max = 10;
    var space = "";
    var stars = "";
    while (y < max) {
        space = "";
        stars = "";
        for (z = 0; z < max - y; z++) space += " ";
        for (z = 0; z < 2 * y + 1; z++) stars += "*";
        console.log(space + stars);
        y++;
    }
    console.log("---------------------");
}

function rhombus() {
    var s,
        a,
        b,
        size = 10,
        board = '',
        out = '';
    
    for (s = 0; s< (size/2)-1; s++){
        out += ' ';
    }
    for(a = 0; a< size/2; a++){
        board += '# ';
        console.log(out, board);
        out = out.substring(0, out.length - 1);
    }
    for(b = size/2; b > 0; b--){
        out += ' ';
        board = board.substring(0, board.length - 2);
        console.log(out, board);
    }
    console.log("---------------------");
}