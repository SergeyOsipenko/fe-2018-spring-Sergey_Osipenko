function avgOfThree(value1, value2, value3) {
	return (value1 + value2 + value3) / 3;
}

function avgOfAny() {	
	var sum = 0;
	var result;
	
	for (var i = 0; i < arguments.length; i++) {
		sum += arguments[i];
	}
	result = sum / arguments.length;
	
	return result;
}

console.log(avgOfThree(4, 6, 8));
console.log(avgOfAny(2, 4, 6, 8));