function factorial(n){
    var result = 1;
    var i = 1;

    do {
        result *=i;
        i++;
    }
    while(i <= n);

    return result;
}

console.log("The factorial: " + factorial(5));