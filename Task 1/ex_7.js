var arr = [];
for (var i = 0; i < 10; i++) {
    arr.push(Math.floor(Math.random() * 99));
}

function maxNum(arr) {
    var max = arr[0];
    for (var i = 1; i < arr.length; i++) {
        if (arr[i] > max) {
            max = arr[i];
        }
    }
    return max;
}

function minNum(arr) {
    var min = arr[0];
    for (var i = 1; i < arr.length; i++) {
        if (arr[i] < min) {
            min = arr[i];
        }
    }
    return min;
}

function sumNum(arr) {
    var sum = 0;
    for (var i = 0; i < arr.length; i++) {
        sum += arr[i];
    }
    return sum;
}

function avgOfArr(arr) {	
    var sum = 0;
    var avg;
    for (var i = 0; i < arr.length; i++) {
        sum += arr[i];
    }
    avg = sum / arr.length;
    return avg;
}

function oddNum(arr) {
    var oddArr = [];
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] % 2 != 0) {
            oddArr.push(arr[i]);
        }	
    }
    return oddArr;
}

console.log("Array:");
console.log(arr);
console.log("The largest value of the array: " + maxNum(arr));
console.log("The smallest value of the array: " + minNum(arr));
console.log("The total sum of the elements: " + sumNum(arr));
console.log("The arithmetic mean of all elements: " + avgOfArr(arr));
console.log("Odd integer values located in array: " + oddNum(arr));