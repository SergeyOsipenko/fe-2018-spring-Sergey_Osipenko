//I used tabs everywhere and edited it on GitLab
function intNumb(a, b) {
    var arr = [];
    while (a <= b) {
        arr.push(a);
        a++;
    }
    return arr;
}

function oddNumb(a, b) {
    var arr = [];
    while (a <= b) {
        if (a % 2 != 0) {
            arr.push(a);
        }
        a++;
    }
    return arr;
}

console.log("Integer numbers:");
console.log(intNumb(2, 14));
console.log();
console.log("Odd integer values: ");
console.log(oddNumb(4, 15));